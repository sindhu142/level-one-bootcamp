#include<stdio.h>
float input(int n)
{
  float a;
  printf("Enter number %d\n",n);
  scanf("%f",&a);
  return a;
}
float calculate_sum(float a,float b)
{
   float result;
   result=a+b;
   return result;
}
float output(float a,float b,float c)
{
   printf("The sum of %f+%f is %f",a,b,c);
}
float main()
{
   float a=input(1);
   float b=input(2);
   float sum=calculate_sum(a,b);
   output(a,b,sum);
   return 0;
}
