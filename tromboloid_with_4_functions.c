#include<stdio.h>
float input(char n[])
{
  float a;
  printf("Enter the %s\n",n);
  scanf("%f",&a);
  return a;
}
float calculate_volume(float h,float d,float b)
{
  float volume;
  volume=(((h*d)+d)/b)*1/3;
  return volume;
}
float output(float h,float d,float b,float volume)
{
  printf("The volume of the tromboloid with height %f,depth %f,breadth %f is %f",h,d,b,volume);
}
float main()
{
  float h=input("height");
  float d=input("depth");
  float b=input("breadth");
  float volume=calculate_volume(h,d,b);
  output(h,d,b,volume);
  return 0;
}
